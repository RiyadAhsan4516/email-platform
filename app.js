const express = require("express");
const morgan = require("morgan");
const UserRoutes = require("./routes/userRoutes");

const app = express();
app.use(express.json());

app.use("/api/v1/rmail", UserRoutes);

module.exports = app;
