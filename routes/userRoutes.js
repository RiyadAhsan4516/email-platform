const express = require("express");
const Router = express.Router();
const UserHandler = require("./../controllers/userController");
const EmailHandler = require("./../controllers/emailController");

Router.post("/signup", UserHandler.signup);
Router.post("/login", UserHandler.login);
Router.get("/inbox", UserHandler.isloggedin, UserHandler.inbox);
Router.get("/sentbox", UserHandler.isloggedin, UserHandler.sentbox);
Router.post("/sendEmail", UserHandler.isloggedin, EmailHandler.sendEmail);

module.exports = Router;
