const { Console } = require("console");
const dotenv = require("dotenv");
dotenv.config({ path: "./config.env" });
const mongoose = require("mongoose");
const app = require("./app");

const db = process.env.DATABASE;

mongoose.set("strictQuery", true);

mongoose
  .connect(db, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log("database connected"));

app.listen(process.env.PORT, process.env.LOCALHOST, () => {
  console.log(`server running on port ${process.env.PORT}`);
});
