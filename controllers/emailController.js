const Emails = require("./../model/emailsModel");
const Users = require("./../model/userModel");

async function saveToRecieved(reciever, emailid) {
  const email = reciever;
  const user = await Users.findOne({ email });
  user.recievedEmails.push(emailid);
  user.save();
}

async function saveToSent(emailid, sender, reciever) {
  const email = sender;
  const user = await Users.findOne({ email });
  user.sentEmails.push(emailid);
  user.save();
  saveToRecieved(reciever, emailid);
}

exports.sendEmail = async (req, res, next) => {
  try {
    req.body.from = req.user.email;
    const email = await Emails.create(req.body);
    saveToSent(email._id, req.user.email, req.body.to);
    res.status(200).json({
      status: "Success",
      message: "email has been sent",
      email,
    });
  } catch {
    res.status(401).json({
      status: "Failed",
      err,
    });
  }
};
