const Users = require("./../model/userModel");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");

const generateToken = function (id) {
  return jwt.sign({ id }, process.env.SECRET, {
    expiresIn: process.env.JWT_EXPIRATION_TIME,
  });
};

exports.signup = async (req, res, next) => {
  try {
    const user = await Users.create(req.body);
    const token = generateToken(user._id);
    user.password = undefined;

    res.status(200).json({
      status: "Success",
      message: "New user created",
      token,
      user,
    });
  } catch (err) {
    res.status(500).json({
      status: "Failed",
      err,
    });
  }
};

exports.login = async (req, res, next) => {
  try {
    const { email, password } = req.body;
    if (!email || !password)
      throw new Error("Please provide all the credentials");
    const user = await Users.findOne({ email }).select("+password");
    if (!user || !(await bcrypt.compare(password, user.password))) {
      throw new Error("email or password incorrect");
    }
    const token = generateToken(user._id);
    res.status(200).json({
      status: "Login successful",
      token,
    });
  } catch (err) {
    res.status(400).json({
      status: "Failed",
      err,
    });
  }
};

exports.isloggedin = async (req, res, next) => {
  try {
    let token;
    if (
      req.headers.authorization &&
      req.headers.authorization.startsWith("Bearer")
    ) {
      token = req.headers.authorization.split(" ")[1];
    }
    if (!token) throw new error("token not found");
    const decoded = jwt.verify(token, process.env.SECRET);
    const user = await Users.findById(decoded.id);
    if (!user) throw new Error("User does not exist");
    else req.user = user;
    next();
  } catch (err) {
    res.status(401).json({
      status: "Failed",
      err,
    });
  }
};

exports.inbox = async (req, res, next) => {
  try {
    const email = req.user.email;
    let user = await Users.findOne({ email }).populate({
      path: "recievedEmails",
      select: "-__v -_id",
    });
    const recieved = user.recievedEmails;
    res.status(200).json({
      status: "Success",
      recieved: user.recievedEmails,
    });
  } catch (err) {
    res.status(401).json({
      status: "Failed",
      err,
    });
  }
};

exports.sentbox = async (req, res, next) => {
  try {
    let user = await Users.findOne({ email: req.user.email }).populate({
      path: "sentEmails",
      select: "-__v -_id",
    });
    res.status(200).json({
      status: "Success",
      sent: user.sentEmails,
    });
  } catch (err) {
    res.status(401).json({
      status: "Failed",
      err,
    });
  }
};
