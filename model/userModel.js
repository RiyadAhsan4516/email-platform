const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "A user must have a name"],
    trim: true,
    maxlength: [50, "A name cannot have more than 50 characters"],
    minlength: [1, "A name cannot have less than 1 characters"],
  },
  email: {
    type: String,
    required: [true, "A user must have an email"],
    unique: [true, "All e-mails must be unique"],
    validate: {
      validator: function (val) {
        return val.split("@")[1] == "rmail.com";
      },
      message: "All emails must end with rmail.com",
    },
  },
  password: {
    type: {},
    required: [true, "Users must have a password"],
    minlength: [8, "Passwords cannot be less than 8 characters long"],
    select: false,
  },
  confirmPassword: {
    type: {},
    validate: {
      validator: function (val) {
        return val == this.password;
      },
      message: "Passwords do not match",
    },
  },
  sentEmails: [
    {
      type: mongoose.Schema.ObjectId,
      ref: "emails",
    },
  ],
  recievedEmails: [
    {
      type: mongoose.Schema.ObjectId,
      ref: "emails",
    },
  ],
});

userSchema.pre("save", async function (next) {
  if (!this.isModified("password")) return next();
  this.password = await bcrypt.hash(this.password, 12);
  this.confirmPassword = undefined;
  next();
});

const User = mongoose.model("users", userSchema);
module.exports = User;
