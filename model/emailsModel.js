const mongoose = require("mongoose");

const emailSchema = new mongoose.Schema({
  from: {
    type: String,
    required: [true, "An email must be sent from an address"],
  },
  to: {
    type: String,
    required: [true, "An email must have a destination"],
  },
  category: {
    type: String,
    enum: {
      values: ["Primary", "Social", "Promotional", "Forum"],
      message: "Only 4 categories of emails are allowed",
    },
    default: "Primary",
  },
  body: {
    type: String,
    required: [true, "An email must have a body"],
  },
});

const Email = mongoose.model("emails", emailSchema);
module.exports = Email;
